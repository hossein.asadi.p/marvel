//
//  Comic.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

struct Comic: Decodable {
    let id: Int
    let title: String
    let issueNumber: Int
    let description: String?
    let resourceURI: String
    let thumbnail: Thumbnail
    let pageCount: Int
    let series: Item
    let creators: CollectionItem
    let characters: CollectionItem
    let prices: [Price]
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case issueNumber
        case description
        case pageCount
        case resourceURI
        case thumbnail
        case series
        case creators
        case characters
        case prices
    }
    
    struct Price: Decodable {
        let type: String
        let price: Double
        
        private enum CodingKeys: String, CodingKey {
            case type
            case price
        }
    }
}
