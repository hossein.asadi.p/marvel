//
//  HeroesListViewController.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol HeroesListViewControllerProtocol: class {
    func heroesListViewControllerDidSelectHero(_ selectedHero : Characters)
    func heroesListViewControllerDidSelectComic(_ selectedComic : Item)
    func heroesListViewControllerBarButtonPressed()
}

class HeroesListViewController: UIViewController {
    
    var page = 1
    var searchText = ""
    private let limit = 20
    private var charactersItems: [Characters] = []
    var cachedPosition = Dictionary<IndexPath,CGPoint>()
    weak var delegate: HeroesListViewControllerProtocol?
    var imageDownloader: ImageDownloadManagerProtocol?
    let characterDataProvider: CharacterDataProviderProtocol
    let comicDataProvider: ComicDataProviderProtocol
    var wantToRead: UIBarButtonItem!
    var storedComicsURI: [String] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    var characters: DataModel<Characters>? {
        didSet{
            tableView.reloadData()
        }
    }
    
    @objc func barButtonaction() {
        delegate?.heroesListViewControllerBarButtonPressed()
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    
    init(characterDataProvider: CharacterDataProviderProtocol, comicDataProvider: ComicDataProviderProtocol) {
        self.characterDataProvider = characterDataProvider
        self.comicDataProvider = comicDataProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: Identifiers.HeroesCell, bundle: nil), forCellReuseIdentifier: Identifiers.HeroesCell)
        tableView.delegate = self
        tableView.dataSource = self
        search.delegate = self
        
        wantToRead = UIBarButtonItem(title: "Want To Reads", style: .plain, target: self, action: #selector(barButtonaction))
        navigationItem.rightBarButtonItem = wantToRead
        
        getCharactersList()
    }
    
    func getCharactersList(isSearch: Bool = false) {
        let offset = (page - 1) * limit
        let vc = LoadingViewController()
        if page == 1 {
            isSearch ? navigationController?.transition(to: tableView, child: vc)
                : navigationController?.transition(child: vc)
        }
        DispatchQueue.global().async {
            let result = (isSearch && self.searchText != "") ? self.characterDataProvider.searchCharacterBy(name: self.searchText, offset: offset, limit: self.limit)
                : self.characterDataProvider.getCharactersList(offset: offset, limit: self.limit)
            DispatchQueue.main.async {
                self.navigationController?.remove(vc)
                switch result {
                case .success(let value):
                    self.updateHeroListStoredComicsURI()
                    if self.page == 1 {
                        self.charactersItems = value.data.results
                        self.characters = value
                    }
                    else {
                        var characters = value
                        characters.data.results = self.charactersItems + characters.data.results
                        self.charactersItems = characters.data.results
                        self.characters = characters
                    }
                case .failure(_):
                    let failedVC = FailedViewController(message: "Failed to load data")
                    failedVC.delegate = self
                    self.navigationController?.transition(child: failedVC)
                }
            }
        }
    }
    
    func updateHeroListStoredComicsURI() {
        let key = Identifiers.storedComicsURI
        if let uris = preferences.value(forKey: key) as? [String] {
            storedComicsURI = uris
        }
    }
    
    func listReachedToEnd() {
        page = page + 1
        getCharactersList()
    }
    
    func comicWantToReadStateChanged(_ resourceURI: String, completion: @escaping (Bool?) -> Void) {
        if comicDataProvider.comicExistsOnStorage(resourceURI: resourceURI) {
            comicDataProvider.deleteComicBy(resourceURI: resourceURI, completion: { success in
                completion(success)
            })
        }
        else {
            DispatchQueue.global().async {
                let result = self.comicDataProvider.getServerComicBy(resourceURI: resourceURI)
                DispatchQueue.main.async {
                    switch result {
                    case .success(let value):
                        if let comic = value.data.results.first {
                            let comicDetail = ComicDetail(comic: comic)
                            self.comicDataProvider.saveComic(comic: comicDetail, completion: { success in
                                completion(success)
                            })
                        }
                        else {
                            completion(false)
                        }
                    default:
                        break
                    }
                }
            }
        }
    }
}
extension HeroesListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters?.data.results.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.HeroesCell, for: indexPath) as! HeroesCell
        if let hero = characters?.data.results[indexPath.row] {
            cell.setup(hero: hero, storedComicsURI: storedComicsURI)
            cell.delegate = self
        }
        if let characters = characters?.data {
            let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
            if indexPath.row == lastRowIndex && lastRowIndex < characters.total - 1 {
                listReachedToEnd()
            }
        }
        cell.collectionView.contentOffset = cachedPosition[indexPath] ?? .zero
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let hero = characters?.data.results[indexPath.row] {
            imageDownloader?.downloadImage(hero.thumbnail.url, indexPath: indexPath, handler: { (image, url, indexPath, error) in
                if let indexPathNew = indexPath, indexPathNew == indexPath, url == hero.thumbnail.url {
                    DispatchQueue.main.async {
                        (cell as! HeroesCell).heroImage.image = image
                    }
                }
            })
        }
        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        if indexPath.row == lastRowIndex && indexPath.row > 0 {
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? HeroesCell {
            cachedPosition[indexPath] = cell.collectionView.contentOffset
        }
    }
}
extension HeroesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let hero = characters?.data.results[indexPath.row] {
            delegate?.heroesListViewControllerDidSelectHero(hero)
        }
    }
}
extension HeroesListViewController: HeroesCellProtocol {
    func heroesCellWantToReadButtonPressed(_ resourceURI: String, completion: @escaping (_ isSuccess: Bool?) -> Void) {
        comicWantToReadStateChanged(resourceURI, completion: { success in
            completion(success)
        })
    }
    
    func heroesCellDidSelectComic(_ selectedComic: Item) {
        delegate?.heroesListViewControllerDidSelectComic(selectedComic)
    }
}
extension HeroesListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        page = 1
        searchText = searchBar.text ?? ""
        if searchText != "" {
            getCharactersList(isSearch: true)
        }
        view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            page = 1
            self.searchText = ""
            getCharactersList(isSearch: true)
        }
    }
}
extension HeroesListViewController: FailedViewControllerDelegate {
    func failedViewControllerTryAgainTapped(_ failedVC: FailedViewController) {
        navigationController?.remove(failedVC)
        getCharactersList()
    }
}
