//
//  Coordinator.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

protocol Coordinator {
    func start()
}
