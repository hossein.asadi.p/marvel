//
//  CollectionItem.swift
//  Marvel
//
//  Created by Hossein on 7/4/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

struct CollectionItem: Decodable {
    let available: Int
    let collectionURI: String
    let returned: Int
    let items: [Item]
    
    enum CodingKeys: String, CodingKey {
        case available = "available"
        case collectionURI = "collectionURI"
        case returned = "returned"
        case items = "items"
    }
    
}
