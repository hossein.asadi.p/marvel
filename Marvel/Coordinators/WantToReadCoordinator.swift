//
//  WantToReadCoordinator.swift
//  Marvel
//
//  Created by Hossein on 7/1/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol WantToReadCoordinatorProtocol: class {
    func wantToReadCoordinatorDidUpdate()
}

class WantToReadCoordinator: Coordinator {
    
    private let presenter: UINavigationController
    private let dataProvider: ComicDataProviderProtocol
    weak var delegate: WantToReadCoordinatorProtocol?
    let wantToReadTableVC: WantToReadTableVC
    var comicDetailCoordinator: ComicDetailCoordinator?
    
    init(presenter: UINavigationController, dataProvider: ComicDataProviderProtocol) {
        self.presenter = presenter
        self.dataProvider = dataProvider
        self.wantToReadTableVC = WantToReadTableVC(dataProvider: dataProvider)
    }
    
    func start() {
        wantToReadTableVC.imageDownloader = ImageDownloadManager.sharedInstance
        wantToReadTableVC.delegate = self
        presenter.pushViewController(wantToReadTableVC, animated: true)
    }
}
extension WantToReadCoordinator: WantToReadTableVCProtocol {
    func wantToReadTableVCDidSelectItem(_ item: ComicDetail) {
        let item = Item(resourceURI: item.resourceURI ?? "", name: item.title ?? "")
        comicDetailCoordinator = ComicDetailCoordinator(presenter: presenter, dataProvider: dataProvider, item: item)
        comicDetailCoordinator?.wantToReadStateDelegate = self
        comicDetailCoordinator?.imageDownloader = ImageDownloadManager.sharedInstance
        comicDetailCoordinator?.start()
    }
    func wantToReadTableVCUpdated() {
        self.delegate?.wantToReadCoordinatorDidUpdate()
    }
}
extension WantToReadCoordinator: ComicDetailCoordinatorProtocol {
    func comicDetailCoordinatorWantToReadStateChanged() {
        wantToReadTableVC.getAllComics()
    }
}
