//
//  HeroDetailViewController.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

class HeroDetailViewController: UIViewController {

    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var heroDescription: UILabel!
    @IBOutlet weak var heroNumberOfComics: UILabel!
    @IBOutlet weak var heroNumberOfSeries: UILabel!
    @IBOutlet weak var heroNumberOfStories: UILabel!
    
    let hero: Characters
    var imageDownloader: ImageDownloadManagerProtocol?
    
    init(hero: Characters) {
        self.hero = hero
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        heroName.text = hero.name
        heroDescription.text = hero.description
        heroNumberOfComics.text = "comics: \(hero.comics.available)"
        heroNumberOfSeries.text = "series: \(hero.series.available)"
        heroNumberOfStories.text = "stories: \(hero.stories.available)"
        imageDownloader?.downloadImage(hero.thumbnail.url, indexPath: nil, handler: { [weak self] (image, url, indexPath, error) in
            if let image = image, url == self?.hero.thumbnail.url {
                DispatchQueue.main.async {
                    self?.heroImage.image = image
                }
            }
        })
    }
    
    
}
