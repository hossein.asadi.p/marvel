//
//  ComicDetailModel.swift
//  Marvel
//
//  Created by Hossein on 6/30/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

class ComicDetail {
    var id: Int!
    var title: String!
    var issueNumber: Int?
    var description: String?
    var resourceURI: String?
    var imageUrl: String?
    var pages: Int?
    var series: String?
    var creators: Int?
    var characters: Int?
    var price: Int?
}
