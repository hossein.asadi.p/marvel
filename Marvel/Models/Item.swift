//
//  Item.swift
//  Marvel
//
//  Created by Hossein on 7/4/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

struct Item: Decodable {
    let resourceURI: String
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case resourceURI = "resourceURI"
        case name = "name"
    }
}
