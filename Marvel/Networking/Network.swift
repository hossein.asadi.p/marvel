//
//  Network.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

class Network: HttpAPI {
    
    private let session: URLSession
    private let decoder = JSONDecoder()
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 3
        configuration.timeoutIntervalForResource = 3
        session = URLSession(configuration: configuration, delegate: nil, delegateQueue: .main)
    }
    
    func execute<T: Decodable>(_ request: APIRequest) -> Result<T> {
        var result: Result<T>!
        let group = DispatchGroup()
        group.enter()
        let task = session.dataTask(with: URL(string: request.urlString!)!) { (data, response, error) in
            if let err = error {
                result = Result.failure(err)
            }
            if let data = data {
                do {
                    let movie = try self.decoder.decode(T.self, from: data)
                    result = Result<T>.success(movie)
                } catch let err {
                    result = Result.failure(err)
                }
            } else {
                result = Result.failure(NSError.init(domain: "No Data", code: 0, userInfo: nil))
            }
            group.leave()
        }
        task.resume()
        group.wait()
        return result
    }
}
