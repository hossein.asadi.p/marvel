//
//  DataProvider.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

protocol CharacterDataProviderProtocol {
    func getCharactersList(offset: Int, limit: Int) -> Result<DataModel<Characters>>
    func getCharacter(withId id: Int) -> Result<DataModel<Characters>>
    func searchCharacterBy(name: String, offset: Int, limit: Int) -> Result<DataModel<Characters>>
}

class CharacterDataProvider: CharacterDataProviderProtocol {
    
    let publicKey = "1afa632fbbc2c3d84e1b5ac43d84aec6"
    let privateKey = KeychainService.loadPassword() ?? ""
    
    private var api: HttpAPI
    init(api: HttpAPI) {
        self.api = api
    }
    
    func getCharactersList(offset: Int, limit: Int) -> Result<DataModel<Characters>> {
        let time = Int(round(Date.timeIntervalSinceReferenceDate))
        let hash = Utility.hash(key: publicKey, privateKey: privateKey, timeStamp: "\(time)")
        let urlString = "\(baseUrl)characters?limit=\(limit)&offset=\(offset)&apikey=\(publicKey)&ts=\(time)&hash=\(hash)".replacingOccurrences(of: " ", with: "%20")
        let request = APIRequest(urlString)
        
        let response: Result<DataModel<Characters>> = api.execute(request)
        return response
    }
    
    func getCharacter(withId id: Int) -> Result<DataModel<Characters>> {
        let time = Int(round(Date.timeIntervalSinceReferenceDate))
        let hash = Utility.hash(key: publicKey, privateKey: privateKey, timeStamp: "\(time)")
        let urlString = "\(baseUrl)characters/\(id)?apikey=\(publicKey)&ts=\(time)&hash=\(hash)".replacingOccurrences(of: " ", with: "%20")
        let request = APIRequest(urlString)
        
        let response: Result<DataModel<Characters>> = api.execute(request)
        return response
    }
    
    func searchCharacterBy(name: String, offset: Int, limit: Int) -> Result<DataModel<Characters>> {
        let time = Int(round(Date.timeIntervalSinceReferenceDate))
        let hash = Utility.hash(key: publicKey, privateKey: privateKey, timeStamp: "\(time)")
         let urlString = "\(baseUrl)characters?name=\(name)&limit=\(limit)&offset=\(offset)&apikey=\(publicKey)&ts=\(time)&hash=\(hash)".replacingOccurrences(of: " ", with: "%20")
        let request = APIRequest(urlString)
        
        let response: Result<DataModel<Characters>> = api.execute(request)
        return response
    }
    
    
}
