//
//  ComicDataProvider.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

import Foundation

protocol ComicDataProviderProtocol {
    func getServerComicBy(resourceURI: String) -> Result<DataModel<Comic>>
    func getWantToReadComics() -> [ComicDetail]
    func comicExistsOnStorage(resourceURI: String) -> Bool
    func getStoredComicBy(resourceURI: String) -> ComicDetail?
    func saveComic(comic: ComicDetail, completion:(_ isSuccess: Bool?) -> Void)
    func deleteComicBy(resourceURI: String, completion:(_ isSuccess: Bool?) -> Void)
    func getComic(resourceURI: String) -> (ComicDetail?,Result<DataModel<Comic>>?)
}

class ComicDataProvider: ComicDataProviderProtocol {
    
    private var api: HttpAPI
    private var storageHelper: StorageHelperProtocol
    init(api: HttpAPI, storageHelper: StorageHelperProtocol) {
        self.api = api
        self.storageHelper = storageHelper
    }
    
    func getComic(resourceURI: String) -> (ComicDetail?,Result<DataModel<Comic>>?) {
        if comicExistsOnStorage(resourceURI: resourceURI) {
            return (getStoredComicBy(resourceURI: resourceURI),nil)
        }
        return (nil, getServerComicBy(resourceURI: resourceURI))
    }
    
    func getServerComicBy(resourceURI: String) -> Result<DataModel<Comic>> {
        let time = Int(round(Date.timeIntervalSinceReferenceDate))
        let publicKey = "1afa632fbbc2c3d84e1b5ac43d84aec6"
        let privateKey = KeychainService.loadPassword() ?? ""
        let hash = Utility.hash(key: publicKey, privateKey: privateKey, timeStamp: "\(time)")
        let urlString = "\(resourceURI)?apikey=\(publicKey)&ts=\(time)&hash=\(hash)".replacingOccurrences(of: " ", with: "%20")
        let request = APIRequest(urlString)
        
        let response: Result<DataModel<Comic>> = api.execute(request)
        return response
    }
    
    func comicExistsOnStorage(resourceURI: String) -> Bool {
        let key = Identifiers.storedComicsURI
        if let uris = preferences.value(forKey: key) as? [String] {
            return uris.contains(resourceURI)
        }
        return false
    }
    
    func getStoredComicBy(resourceURI: String) -> ComicDetail? {
        return storageHelper.getComicBy(resourceURI: resourceURI)
    }
    
    func getWantToReadComics() -> [ComicDetail] {
        return storageHelper.getAllComics()
    }
    
    func deleteComicBy(resourceURI: String, completion:(_ isSuccess: Bool?) -> Void) {
        storageHelper.deleteComicBy(resourceURI: resourceURI, completion: { success in
            completion(success)
        })
    }
    
    func saveComic(comic: ComicDetail, completion: (Bool?) -> Void) {
        storageHelper.saveComic(comic: comic, completion: { success in
            completion(success)
        })
    }

}
