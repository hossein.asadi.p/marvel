//
//  ComicDetail.swift
//  Marvel
//
//  Created by Hossein on 6/30/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

class ComicDetail {
    var id: Int64!
    var title: String!
    var issueNumber: Int16?
    var description: String?
    var resourceURI: String?
    var imageUrl: String?
    var pages: Int16?
    var series: String?
    var creators: Int16?
    var characters: Int16?
    var comicPrice: Double?
    var addedToWantToRead = false
    
    convenience init(comic: WantToRead) {
        self.init()
        self.id = comic.id
        self.title = comic.title
        self.issueNumber = comic.issueNumber
        self.description = comic.comicDescription
        self.resourceURI = comic.resourceURI
        self.characters = comic.characters
        self.creators = comic.creators
        self.series = comic.series
        self.imageUrl = comic.imageUrl
        self.pages = comic.pages
        self.comicPrice = Double(round(100*(comic.comicPrice))/100)
        self.addedToWantToRead = true
    }
    
    convenience init(comic: Comic) {
        self.init()
        self.id = Int64(comic.id)
        self.imageUrl = comic.thumbnail.url?.absoluteString
        self.title = comic.title
        self.comicPrice = Double(round(100*(comic.prices.first?.price ?? 0))/100)
        self.pages = Int16(comic.pageCount)
        self.issueNumber = Int16(comic.issueNumber)
        self.characters = Int16(comic.characters.available)
        self.creators = Int16(comic.creators.available)
        self.series = comic.series.name
        self.description = comic.description
        self.resourceURI = comic.resourceURI
        self.addedToWantToRead = false
    }
}

