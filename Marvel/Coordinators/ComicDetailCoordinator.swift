//
//  ComicDetailCoordinator.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol ComicDetailCoordinatorProtocol: class {
    func comicDetailCoordinatorWantToReadStateChanged()
}

class ComicDetailCoordinator: Coordinator {
    
    private let presenter: UINavigationController
    private let dataProvider: ComicDataProviderProtocol
    private let item: Item
    var imageDownloader: ImageDownloadManagerProtocol?
    weak var wantToReadStateDelegate: ComicDetailCoordinatorProtocol?
    let comicDetailVC: ComicDetailViewController
    
    init(presenter: UINavigationController, dataProvider: ComicDataProviderProtocol, item: Item) {
        self.presenter = presenter
        self.dataProvider = dataProvider
        self.item = item
        comicDetailVC = ComicDetailViewController(item: item, dataProvider: dataProvider)
    }
    
    func start() {
        comicDetailVC.imageDownloader = imageDownloader
        comicDetailVC.delegate = self
        presenter.pushViewController(comicDetailVC, animated: true)
    }
}
extension ComicDetailCoordinator: ComicDetailViewControllerProtocol {
    func ComicDetailViewControllerDidPressWantToRead() {
        wantToReadStateDelegate?.comicDetailCoordinatorWantToReadStateChanged()
    }
}
