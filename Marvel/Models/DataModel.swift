//
//  DataModel.swift
//  Marvel
//
//  Created by Hossein on 7/4/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

struct DataModel<T : Decodable>: Decodable {
    
    struct ResponseData<U : Decodable> : Decodable {
        let offset: Int
        let limit: Int
        let count: Int
        let total: Int
        var results : [U]
        
        private enum CodingKeys: String, CodingKey {
            case offset
            case limit
            case count
            case total
            case results = "results"
        }
        
    }
    
    let code : Int
    let status : String
    var data : ResponseData<T>
    
    private enum CodingKeys: String, CodingKey {
        case code
        case status
        case data = "data"
    }
}
