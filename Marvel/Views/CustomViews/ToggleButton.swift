//
//  ToggleButton.swift
//  Marvel
//
//  Created by Hossein on 6/30/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol ToggleButtonProtocol: class {
    func toggleButtonDidPressed(_ button: ToggleButton)
}

@IBDesignable public class ToggleButton: UIButton {
    
    weak var delegate: ToggleButtonProtocol?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
    }
    
    @objc func buttonPressed(sender: ToggleButton) {
        delegate?.toggleButtonDidPressed(sender)
    }
    
    @IBInspectable var setSelected: Bool = false {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var title: String = "Want To Read" {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var selectedTitle: String = "added to Read List" {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var titleTextColor: UIColor = .blue {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var selectedTitleTextColor: UIColor = .white {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var color: UIColor = .white {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var selectedColor: UIColor = .blue {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var borderColor: UIColor = .blue {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var selectedBorderColor: UIColor = .blue {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var radius: CGFloat = 6.0 {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet{
            updateUI()
        }
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        updateUI()
    }
    
    func updateUI() {
        self.setTitle(setSelected ? selectedTitle : title, for: [])
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = setSelected ? selectedBorderColor.cgColor : borderColor.cgColor
        self.backgroundColor = setSelected ? selectedColor : color
        self.setTitleColor(setSelected ? selectedTitleTextColor : titleTextColor, for: [])
    }
    
}

