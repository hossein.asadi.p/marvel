//
//  Thumbnail.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

struct Characters: Decodable {
    let id: Int
    let name: String
    let description: String
    let resourceURI: String
    let thumbnail: Thumbnail
    let comics: CollectionItem
    let series: CollectionItem
    let stories: CollectionItem
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case resourceURI
        case thumbnail
        case comics
        case series
        case stories
    }
    
}
