//
//  ComicDetailViewController.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol ComicDetailViewControllerProtocol: class {
    func ComicDetailViewControllerDidPressWantToRead()
}

class ComicDetailViewController: UIViewController {
    
    let item: Item
    let dataProvider: ComicDataProviderProtocol
    var imageDownloader: ImageDownloadManagerProtocol?
    weak var delegate: ComicDetailViewControllerProtocol?
    var comic: ComicDetail? {
        didSet{
            bindUI()
        }
    }
    
    @IBOutlet weak var comicImage: UIImageView!
    @IBOutlet weak var comicTitle: UILabel!
    @IBOutlet weak var numberOfPages: UILabel!
    @IBOutlet weak var issueNumber: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var series: UILabel!
    @IBOutlet weak var characters: UILabel!
    @IBOutlet weak var creators: UILabel!
    @IBOutlet weak var comicDescription: UILabel!
    @IBOutlet weak var wantToReadButton: ToggleButton!
    
    @IBAction func wantToReadAction(_ sender: Any) {
        wantToRead(for: comic, completion: { [weak self] success in
            if success ?? false {
                self?.comic?.addedToWantToRead = !(sender as! ToggleButton).setSelected
                (sender as! ToggleButton).setSelected = !(sender as! ToggleButton).setSelected
            }
        })
    }
    
    init(item: Item, dataProvider: ComicDataProviderProtocol) {
        self.item = item
        self.dataProvider = dataProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getComic()
    }
    
    func getComic() {
        let vc = LoadingViewController()
        navigationController?.transition(child: vc)
        DispatchQueue.global().async {
            let (comicDetail,result) = self.dataProvider.getComic(resourceURI: self.item.resourceURI)
            DispatchQueue.main.async {
                self.navigationController?.remove(vc)
                if comicDetail != nil {
                    self.comic = comicDetail
                }
                else {
                    switch result {
                    case .success(let value)?:
                        self.comic = self.transformComicModel(comic: value.data.results.first)
                    case .failure(_)?:
                        let failedVC = FailedViewController(message: "Failed to load data")
                        failedVC.delegate = self
                        self.navigationController?.transition(child: failedVC)
                    case .none:
                        break
                    }
                }
            }
        }
    }
    
    func transformComicModel(comic: Comic?) -> ComicDetail? {
        var comicDetail: ComicDetail?
        if let comic = comic {
            comicDetail = ComicDetail(comic: comic)
        }
        return comicDetail
    }
    
    func bindUI() {
        if let comic = comic, comicTitle != nil {
            comicTitle.text = comic.title
            numberOfPages.text = "pages: \(comic.pages ?? 0)"
            issueNumber.text = "issue: \(comic.issueNumber ?? 0)"
            price.text = "price: \(comic.comicPrice ?? 0)"
            series.text = "series: \(comic.series ?? "")"
            characters.text = "characters: \(comic.characters ?? 0)"
            creators.text = "creators: \(comic.creators ?? 0)"
            comicDescription.text = comic.description
            let imageUrl = URL(string: comic.imageUrl ?? "")
            imageDownloader?.downloadImage(imageUrl, indexPath: nil, handler: { [weak self] (image, url, indexPath, error) in
                if let image = image, url == imageUrl {
                    DispatchQueue.main.async {
                        self?.comicImage.image = image
                    }
                }
            })
            comic.addedToWantToRead ? (wantToReadButton.setSelected = true) : (wantToReadButton.setSelected = false)
        }
    }
    
    func wantToRead(for comic: ComicDetail?, completion: (Bool?) -> Void) {
        if let comic = comic {
            if comic.addedToWantToRead {
                dataProvider.deleteComicBy(resourceURI: comic.resourceURI ?? "", completion: { (success) in
                    completion(success)
                })
            }
            else {
                dataProvider.saveComic(comic: comic, completion: { (success) in
                    completion(success)
                })
            }
            delegate?.ComicDetailViewControllerDidPressWantToRead()
        }
        else {
            completion(false)
        }
    }
}
extension ComicDetailViewController: FailedViewControllerDelegate {
    func failedViewControllerTryAgainTapped(_ failedVC: FailedViewController) {
        getComic()
    }
}

