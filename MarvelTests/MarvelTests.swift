//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import XCTest
@testable import Marvel

class MarvelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUtilityHash() {
        let hash = Utility.hash(key: "test", privateKey: "key", timeStamp: "ts")
        let expectedResult = "4c9e8ae6ff8a7006626d2a569a1a6f70"
        XCTAssertEqual(hash, expectedResult)
    }
    
    func testGetCharactersList() {
        let api = MockNetwork(fileName: "characters")
        let dataProvider = CharacterDataProvider(api: api)
        let result = dataProvider.getCharactersList(offset: 0, limit: 20)
        switch result {
        case .success(let value):
            XCTAssertEqual(value.data.results.count, 20)
        default:
            break
        }
    }
    
    func testGetComicById() {
        let api = MockNetwork(fileName: "comicByID")
        let dataProvider = ComicDataProvider(api: api, storageHelper: CoreDataHelper.instance)
        let result = dataProvider.getServerComicBy(resourceURI: "http://gateway.marvel.com/v1/public/comics/24571")
        switch result {
        case .success(let value):
            XCTAssertEqual(value.data.results.first?.creators.available, 7)
        default:
            break
        }
    }
    
}
