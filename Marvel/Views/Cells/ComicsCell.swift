//
//  ComicsCell.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol ComicsCellProtocol: class {
    func wantToReadButtonPressed(sender: ToggleButton, resourceURI: String)
}

class ComicsCell: UICollectionViewCell {

    weak var delegate: ComicsCellProtocol?
    var resourceURI: String?
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var comicName: UILabel!
    @IBOutlet weak var addToWantToReadButton: ToggleButton!
    
    @IBAction func addToWantToRead(_ sender: Any) {
        if let uri = resourceURI {
            delegate?.wantToReadButtonPressed(sender: sender as! ToggleButton, resourceURI: uri)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        mainView.layer.cornerRadius = 5
    }

}
