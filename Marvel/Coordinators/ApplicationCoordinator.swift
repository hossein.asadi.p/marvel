//
//  ApplicationCoordinator.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation
import UIKit

class ApplicationCoordinator: Coordinator {
    
    let characterDataProvider: CharacterDataProviderProtocol
    let comicDataProvider: ComicDataProviderProtocol
    let window: UIWindow
    let rootViewController: UINavigationController
    let heroesListCoordinator: HeroesListCoordinator
    
    init(window: UIWindow) {
        self.window = window
        let api = Network()
        characterDataProvider = CharacterDataProvider(api: api)
        comicDataProvider = ComicDataProvider(api: api, storageHelper: CoreDataHelper.instance)
        rootViewController = UINavigationController()
        
        heroesListCoordinator = HeroesListCoordinator(presenter: rootViewController, characterDataProvider: characterDataProvider, comicDataProvider: comicDataProvider)
        heroesListCoordinator.imageDownloader = ImageDownloadManager.sharedInstance
        
        let privateKey = "7446208a09eaa9fe70d795d458a6a51b6077ec4a"
        KeychainService.savePassword(token: privateKey as NSString)
    }
    
    func start() {
        window.rootViewController = rootViewController
        heroesListCoordinator.start()
        window.makeKeyAndVisible()
    }
}
