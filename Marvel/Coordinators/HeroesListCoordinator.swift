//
//  HeroesListCoordinator.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation
import UIKit

class HeroesListCoordinator: Coordinator {
    private let presenter: UINavigationController
    let characterDataProvider: CharacterDataProviderProtocol
    let comicDataProvider: ComicDataProviderProtocol
    let heroListVC: HeroesListViewController
    var imageDownloader: ImageDownloadManagerProtocol?
    var heroDetailCoordinator: HeroDetailCoordinator?
    var comicDetailCoordiantor: ComicDetailCoordinator?
    var wantToReadCoordinator: WantToReadCoordinator?
    
    init(presenter: UINavigationController, characterDataProvider: CharacterDataProviderProtocol, comicDataProvider: ComicDataProviderProtocol) {
        self.presenter = presenter
        self.characterDataProvider = characterDataProvider
        self.comicDataProvider = comicDataProvider
        heroListVC = HeroesListViewController(characterDataProvider: characterDataProvider, comicDataProvider: comicDataProvider)
    }
    
    func start() {
        heroListVC.delegate = self
        heroListVC.imageDownloader = imageDownloader
        presenter.pushViewController(heroListVC, animated: true)
    }
}
extension HeroesListCoordinator: HeroesListViewControllerProtocol {
    func heroesListViewControllerDidSelectHero(_ selectedHero: Characters) {
        heroDetailCoordinator = HeroDetailCoordinator(presenter: presenter, hero: selectedHero)
        heroDetailCoordinator?.imageDownloader = imageDownloader
        heroDetailCoordinator?.start()
    }
    
    func heroesListViewControllerDidSelectComic(_ selectedComic: Item) {
        comicDetailCoordiantor = ComicDetailCoordinator(presenter: presenter, dataProvider: comicDataProvider, item: selectedComic)
        comicDetailCoordiantor?.wantToReadStateDelegate = self
        comicDetailCoordiantor?.imageDownloader = imageDownloader
        comicDetailCoordiantor?.start()
    }
    func heroesListViewControllerBarButtonPressed() {
        wantToReadCoordinator = WantToReadCoordinator(presenter: presenter, dataProvider: comicDataProvider)
        wantToReadCoordinator?.delegate = self
        wantToReadCoordinator?.start()
    }
}
extension HeroesListCoordinator: ComicDetailCoordinatorProtocol {
    func comicDetailCoordinatorWantToReadStateChanged() {
        heroListVC.updateHeroListStoredComicsURI()
    }
}
extension HeroesListCoordinator: WantToReadCoordinatorProtocol {
    func wantToReadCoordinatorDidUpdate() {
        heroListVC.updateHeroListStoredComicsURI()
    }
}
