//
//  MockNetwork.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

class MockNetwork: HttpAPI {
    
    let fileName: String
    private let decoder = JSONDecoder()
    
    init(fileName: String) {
        self.fileName = fileName
    }
    
    func execute<T: Decodable>(_ request: APIRequest) -> Result<T> {
        var result: Result<T>!
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let movie = try self.decoder.decode(T.self, from: data)
                result = Result<T>.success(movie)
            } catch let err {
                result = Result.failure(err)
            }
        }
        else {
            result = Result.failure(NSError.init(domain: "No Data", code: 0, userInfo: nil))
        }
        return result
    }
}
