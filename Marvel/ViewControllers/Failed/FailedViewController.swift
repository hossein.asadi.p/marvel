//
//  FailedViewController.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol FailedViewControllerDelegate: class {
    func failedViewControllerTryAgainTapped(_ failedVC: FailedViewController)
}

class FailedViewController: UIViewController {
    
    @IBOutlet var label: UILabel?
    private var message: String
    weak var delegate: FailedViewControllerDelegate?

    required init?(coder aDecoder: NSCoder) { fatalError("...") }
    
    init(message: String) {
        self.message = message
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.label?.text = message
    }
    
    @IBAction func tryAgainAction(_ sender: UIButton) {
        delegate?.failedViewControllerTryAgainTapped(self)
    }
}
