//
//  HttpAPI.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

protocol HttpAPI {
    func execute<T: Decodable>(_ request: APIRequest) -> Result<T>
}

enum Result<T> {
    case success(T)
    case failure(Error)
}

class APIResponse<T: Any>
{
    var data : T?
    var error: Error?
    
    init(data: T?, error: Error?) {
        self.data = data
        self.error = error
    }
}

class APIRequest
{
    var urlString: String?

    init(_ path: String) {
        self.urlString = path.replacingOccurrences(of: " ", with: "%20")
    }
}
