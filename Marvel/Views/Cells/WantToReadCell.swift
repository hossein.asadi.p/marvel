//
//  WantToReadCell.swift
//  Marvel
//
//  Created by Hossein on 6/30/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

class WantToReadCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        mainView.layer.borderWidth = 1
        mainView.layer.cornerRadius = 5
        mainView.clipsToBounds = true
        itemImage.layer.cornerRadius = 5
        itemImage.clipsToBounds = true
        itemImage.image = UIImage(named: "marvel.jpg")
    }
    
}
