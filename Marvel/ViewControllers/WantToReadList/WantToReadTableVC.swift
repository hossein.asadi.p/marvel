//
//  WantToReadTableVC.swift
//  Marvel
//
//  Created by Hossein on 7/1/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol WantToReadTableVCProtocol: class {
    func wantToReadTableVCDidSelectItem(_ item: ComicDetail)
    func wantToReadTableVCUpdated()
}

class WantToReadTableVC: UITableViewController {
    
    let dataProvider: ComicDataProviderProtocol
    var imageDownloader: ImageDownloadManagerProtocol?
    weak var delegate: WantToReadTableVCProtocol?
    var items: [ComicDetail] = []
    var deleteButton: UIBarButtonItem!
    
    init(dataProvider: ComicDataProviderProtocol) {
        self.dataProvider = dataProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: Identifiers.WantToReadCell, bundle: nil), forCellReuseIdentifier: Identifiers.WantToReadCell)
        deleteButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(WantToReadTableVC.showEditing(sender:)))
        navigationItem.rightBarButtonItem = deleteButton
        getAllComics()
    }

    func getAllComics() {
        let vc = LoadingViewController()
        navigationController?.transition(child: vc)
        let comics = dataProvider.getWantToReadComics()
        DispatchQueue.main.async {
            self.navigationController?.remove(vc)
            self.set(items: comics)
            if !comics.isEmpty{
                self.navigationController?.navigationItem.rightBarButtonItem = self.deleteButton
            }
            self.delegate?.wantToReadTableVCUpdated()
        }
    }
    
    func delete(_ item: ComicDetail) {
        dataProvider.deleteComicBy(resourceURI: item.resourceURI ?? "", completion: { [weak self] success in
            if success ?? false {
                self?.delegate?.wantToReadTableVCUpdated()
            }
        })
    }
    
    func set(items: [ComicDetail]) {
        self.items = items
        tableView.reloadData()
    }
    
    @objc func showEditing(sender: UIBarButtonItem)
    {
        navigationItem.rightBarButtonItem?.title = tableView.isEditing ?
            "Edit" : "Done"
        tableView.isEditing = !tableView.isEditing
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.WantToReadCell, for: indexPath) as! WantToReadCell
        cell.itemTitle.text = items[indexPath.row].title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        let imageUrl = URL(string: item.imageUrl ?? "")
        imageDownloader?.downloadImage(imageUrl, indexPath: indexPath, handler: { (image, url, indexPath, error) in
            if let indexPathNew = indexPath, indexPathNew == indexPath, url == imageUrl {
                DispatchQueue.main.async {
                    (cell as! WantToReadCell).itemImage.image = image
                }
            }
        })
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.wantToReadTableVCDidSelectItem(items[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            delete(item)
        }
    }
}
