//
//  HeroDetailCoordinator.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

class HeroDetailCoordinator: Coordinator {
    
    private let presenter: UINavigationController
    let hero: Characters
    var imageDownloader: ImageDownloadManagerProtocol?
    
    init(presenter: UINavigationController, hero: Characters) {
        self.presenter = presenter
        self.hero = hero
    }
    
    func start() {
        let heroDetailVC = HeroDetailViewController(hero: hero)
        heroDetailVC.imageDownloader = imageDownloader
        presenter.pushViewController(heroDetailVC, animated: true)
    }
}
