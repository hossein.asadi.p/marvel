//
//  Thumbnail.swift
//  Marvel
//
//  Created by Hossein on 7/4/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation

struct Thumbnail: Decodable {
    let path: String
    let pathExtension: String
    
    private enum CodingKeys: String, CodingKey {
        case path
        case pathExtension = "extension"
    }
    
    var url : URL? {
        return URL(string: "\(path).\(pathExtension)")
    }
}
