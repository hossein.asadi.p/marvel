//
//  Common.swift
//  Marvel
//
//  Created by Hossein on 7/2/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

let preferences = UserDefaults.standard
let baseUrl = "http://gateway.marvel.com/v1/public/"

struct Identifiers
{
    static let storedComicsURI = "storedComicsURI"
    static let coreDataWantToReadEntityName = "WantToRead"
    static let ComicsCell = "ComicsCell"
    static let HeroesCell = "HeroesCell"
    static let WantToReadCell = "WantToReadCell"
}
