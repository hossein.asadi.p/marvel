//
//  Router.swift
//  Marvel
//
//  Created by Hossein on 6/29/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    public var rootViewController: UIViewController? {
        return viewControllers.first
    }
    
    public var lastViewController: UIViewController? {
        return viewControllers.last
    }
    
    func remove(_ viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
    
    func transition(to containerView: UIView? = nil, duration: Double = 0.25, child: UIViewController, completion: ((Bool) -> Void)? = nil) {
        
        let container = ((containerView != nil) ? containerView! : lastViewController?.view!)
        
        let current = lastViewController?.childViewControllers.last
        lastViewController?.addChildViewController(child)
        
        let newView = child.view!
        newView.translatesAutoresizingMaskIntoConstraints = true
        newView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        newView.frame = (container?.bounds)!
        
        if let existing = current {
            existing.willMove(toParentViewController: nil)
            
            lastViewController?.transition(from: existing, to: child, duration: duration, options: [.transitionCrossDissolve], animations: { }, completion: { [unowned self] done in
                existing.removeFromParentViewController()
                child.didMove(toParentViewController: self.lastViewController)
                completion?(done)
            })
        } else {
            container?.addSubview(newView)
            
            UIView.animate(withDuration: duration, delay: 0, options: [.transitionCrossDissolve], animations: { }, completion: { done in
                child.didMove(toParentViewController: self.lastViewController)
                completion?(done)
            })
        }
    }
}
