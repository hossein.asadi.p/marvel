//
//  HeroesCell.swift
//  Marvel
//
//  Created by Hossein on 6/28/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import UIKit

protocol HeroesCellProtocol: class {
    func heroesCellDidSelectComic(_ selectedComic : Item)
    func heroesCellWantToReadButtonPressed(_ resourceURI: String, completion: @escaping (_ isSuccess: Bool?) -> Void)
}
extension HeroesCellProtocol {
    func heroesCellWantToReadButtonPressed(_ resourceURI: String, completion: @escaping (_ isSuccess: Bool?) -> Void) {}
}

class HeroesCell: UITableViewCell {
    
    var scrollToIndex: IndexPath?
    weak var delegate : HeroesCellProtocol?
    var storedComicsURI : [String] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    var comics: [Item] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var heroDescription: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: Identifiers.ComicsCell, bundle: nil), forCellWithReuseIdentifier: Identifiers.ComicsCell)
        collectionView.delegate = self
        collectionView.dataSource = self
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        mainView.layer.borderWidth = 1
        mainView.layer.cornerRadius = 5
        mainView.clipsToBounds = true
        heroImage.layer.cornerRadius = 5
        heroImage.clipsToBounds = true
        heroImage.image = UIImage(named: "marvel.jpg")
    }
    
    func setup(hero: Characters, storedComicsURI: [String] = []) {
        heroName.text = hero.name
        heroDescription.text = hero.description
        
        self.storedComicsURI = storedComicsURI
        comics = hero.comics.items
        comics.count == 0 ? (collectionView.isHidden = true) : (collectionView.isHidden = false)
        collectionView.reloadData()
        
        if let indexPath = scrollToIndex {
            collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
        }
    }
}
extension HeroesCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.ComicsCell, for: indexPath) as! ComicsCell
        cell.comicName.text = comics[indexPath.row].name
        cell.delegate = self
        cell.resourceURI = comics[indexPath.row].resourceURI
        if storedComicsURI.contains(comics[indexPath.row].resourceURI) {
            cell.addToWantToReadButton.setSelected = true
        }
        else {
            cell.addToWantToReadButton.setSelected = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.heroesCellDidSelectComic(comics[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 220, height: 110)
    }
}
extension HeroesCell: ComicsCellProtocol {
    func wantToReadButtonPressed(sender: ToggleButton, resourceURI: String) {
        delegate?.heroesCellWantToReadButtonPressed(resourceURI, completion: { success in
            if success ?? false {
                sender.setSelected = !sender.setSelected
            }
        })
    }
}
