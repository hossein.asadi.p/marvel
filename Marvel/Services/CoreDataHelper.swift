//
//  CoreDataHelper.swift
//  Marvel
//
//  Created by Hossein on 6/30/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol StorageHelperProtocol: class {
    func getAllComics() -> [ComicDetail]
    func getComicBy(resourceURI: String) -> ComicDetail?
    func saveComic(comic: ComicDetail, completion:(_ isSuccess: Bool?) -> Void)
    func deleteComicBy(resourceURI: String, completion:(_ isSuccess: Bool?) -> Void)
}

class CoreDataHelper: StorageHelperProtocol {
    
    let entityName = Identifiers.coreDataWantToReadEntityName
    let storedComicsUrikey = Identifiers.storedComicsURI
    static let instance = CoreDataHelper()
    private init() {
    }
    
    func getContext() -> NSManagedObjectContext
    {
        return (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    }
    
    func getAllComics() -> [ComicDetail]
    {
        var comicArr = Array<ComicDetail>()
        var results:[AnyObject]?
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do{
            results = try context.fetch(fetchRequest) as! [WantToRead]
            for item in results!
            {
                let comicDetail = ComicDetail(comic: item as! WantToRead)
                comicArr.append(comicDetail)
            }
            return comicArr
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        return comicArr
    }
    
    func getComicBy(resourceURI: String) -> ComicDetail?
    {
        var comic: ComicDetail?
        var results: [AnyObject]?
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do{
            results = try context.fetch(fetchRequest) as! [WantToRead]
            for item in results!
            {
                if (item as! WantToRead).resourceURI == resourceURI {
                    comic = ComicDetail(comic: item as! WantToRead)
                    break
                }
                
            }
            return comic
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        return comic
    }
    
    func deleteComicBy(resourceURI: String, completion:(_ isSuccess: Bool?) -> Void)
    {
        var comic: WantToRead?
        var results: [AnyObject]?
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do{
            results = try context.fetch(fetchRequest) as! [WantToRead]
            for item in results!
            {
                if (item as! WantToRead).resourceURI == resourceURI {
                    comic = (item as! WantToRead)
                    break
                }
                
            }
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        if let comic = comic {
            context.delete(comic)
            
            var comicsURI: [String] = preferences.value(forKey: storedComicsUrikey) as! [String]
            if let index = comicsURI.index(of: comic.resourceURI!) {
                comicsURI.remove(at: index)
            }
            preferences.set(comicsURI, forKey: storedComicsUrikey)
        }
        else {
            completion(false)
        }
        do {
            try context.save()
            completion(true)
        }
        catch let error as NSError{
            print(error.localizedDescription)
            completion(false)
        }
    }
    
    func saveComic(comic: ComicDetail, completion:(_ isSuccess: Bool?) -> Void)
    {
        let context = getContext()
        let record = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! WantToRead
        
        record.id = comic.id
        record.imageUrl = comic.imageUrl
        record.title = comic.title
        record.comicPrice = comic.comicPrice ?? 0
        record.pages = comic.pages ?? 0
        record.issueNumber = comic.issueNumber ?? 0
        record.characters = comic.characters ?? 0
        record.creators = comic.creators ?? 0
        record.series = comic.series
        record.comicDescription = comic.description
        record.resourceURI = comic.resourceURI
        
        do {
            try context.save()
            var comicsURI: [String] = []
            if (preferences.value(forKey: storedComicsUrikey) as? [String]) != nil {
                comicsURI = preferences.value(forKey: storedComicsUrikey) as! [String]
            }
            comicsURI.append(comic.resourceURI!)
            preferences.set(comicsURI, forKey: storedComicsUrikey)
            
            completion(true)
        }
        catch let error as NSError{
            print(error.localizedDescription)
            completion(false)
        }
        
    }
    
}
