//
//  WantToRead+CoreDataProperties.swift
//  Marvel
//
//  Created by Hossein on 6/30/19.
//  Copyright © 2019 CafeBazaar. All rights reserved.
//
//

import Foundation
import CoreData


extension WantToRead {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WantToRead> {
        return NSFetchRequest<WantToRead>(entityName: Identifiers.coreDataWantToReadEntityName)
    }

    @NSManaged public var id: Int64
    @NSManaged public var comicPrice: Double
    @NSManaged public var title: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var pages: Int16
    @NSManaged public var issueNumber: Int16
    @NSManaged public var creators: Int16
    @NSManaged public var characters: Int16
    @NSManaged public var series: String?
    @NSManaged public var comicDescription: String?
    @NSManaged public var resourceURI: String?

}
